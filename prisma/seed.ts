import { PermisionSeeds } from './seeder/permisions.seeds';
import { UsersSeeds } from './seeder/users.seeds';
import { PrismaClient } from '@prisma/client';


const prisma = new PrismaClient();

async function main() {
    for (let user of UsersSeeds) {
        await prisma.user.create({
            data: user
        })
    }

    for (let permision of PermisionSeeds) {
        await prisma.permision.create({
            data: permision
        })
    }
}

main().catch(e =>{console.log(e);process.exit(1);}).finally(()=>{prisma.$disconnect();})