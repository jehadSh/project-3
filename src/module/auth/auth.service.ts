import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { loginDto } from './dto/login.dto';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) { }

  async signIn(signInDto: loginDto) {
    const user = await this.usersService.getUser(signInDto.email);
    if (!user || user.password !== signInDto.password) {
      throw new UnauthorizedException();
    }
    const payload = { email: user.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
      user
    };
  }

  
}