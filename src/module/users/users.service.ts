import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { updateDto } from './dto/update.dto';

@Injectable()

export class UsersService {
  private readonly prisma: PrismaClient;


  constructor() {
    this.prisma = new PrismaClient();
  }

  // this function for login 
  async getUser(email: string) {
    return await this.prisma.user.findFirst({
      where: {
        email: email
      }
    });
  }


  // all this function for CRUD User
  async findAll() {
    return await this.prisma.user.findMany();
  }

  async createUser(createDto): Promise<string> {
    await this.prisma.user.create({
      data: {
        email: createDto.email,
        password: createDto.password,
        name: createDto.name
      }
    })
    return "The user has been added successfully"
  }

  async findOne(id) {
    return await this.prisma.user.findFirst(id);
  }

  async updateUser(userId, updateDto) {
    const user = await this.prisma.user.findFirst(userId);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return await this.prisma.user.update({
      where: {
        id: user.id
      },
      data: {
        name: updateDto.name,
        email:updateDto.email,
        password:updateDto.password
      }
    });
  }

  async delete(id) {
    console.log("ljfsdkljdfksjdflksjdfksljdflksdf");
    return await this.prisma.user.delete({
      where: {
        id: id
      },
    });
  }

}