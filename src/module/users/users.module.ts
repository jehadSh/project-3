import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserController } from './users.controller';
import { TestController } from './test.controller';

@Module({
  providers: [UsersService],
  exports: [UsersService],
  controllers: [UserController ,TestController]
})
export class UsersModule { }
