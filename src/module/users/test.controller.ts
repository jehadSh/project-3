import { Body, Controller, Get } from '@nestjs/common';
import { UsersService } from './users.service';



@Controller('test')
export class TestController {

  constructor(private userService: UsersService) { }

  @Get('all')
  findAll() {
    return this.userService.findAll();
  }
}