import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '../auth/auth.guard';
import { createDto } from './dto/create.dto';
import { updateDto } from './dto/update.dto';
import { ApiTags } from '@nestjs/swagger';

// @UseGuards(AuthGuard)
@ApiTags('user')
@Controller('user')
export class UserController {

  constructor(private userService: UsersService) { }


  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @Post()
  Create(@Body() createDto: createDto) {
    return this.userService.createUser(createDto);
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.userService.findOne(id);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() updateDto: updateDto) {
    return this.userService.updateUser(id, updateDto);
  }

  @Delete(':id')
  delete(@Param('id') id) {
    return this.userService.delete(id);
  }

}
