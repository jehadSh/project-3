import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

export function swagger(app: INestApplication) {
    const document = createDocument(app);
    SwaggerModule.setup("swagger", app, document);
}

const ENV = process.env.NODE_ENV ?? "Dev";

function createDocument(app: INestApplication) {
    const config = new DocumentBuilder()
        .setTitle(`Jehad ${ENV}`)
        .setVersion('1.0')
        .addBearerAuth()
        .build();
    const document = SwaggerModule.createDocument(app, config);
    return document;
}