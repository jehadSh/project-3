import { INestApplication, Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient as PrismaBaseClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaBaseClient implements OnModuleInit {
  constructor() {
    const log = ['error'];
    super({
      log: log as any,
    });
  }
  
  async onModuleInit() {
    await this.$connect();
  }

  async enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }
}
