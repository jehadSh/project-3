import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { swagger } from './swagger';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { PrismaService } from './Common/Prisma/prisma.service';

function enablePrismaShutdownHooks(app: INestApplication) {
  /**
   * needed to force prisma to wait for nest to close the app
   * @see https://docs.nestjs.com/recipes/prisma#issues-with-enableshutdownhooks
   */
  app.get(PrismaService).enableShutdownHooks(app);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  swagger(app);
  enablePrismaShutdownHooks(app);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true
    }),
  );
  await app.listen(3000);
}
bootstrap();
